use std::process::Command;
use std::env;


fn windows() {
    Command::new( "explorer" )
        .arg( "." )
        .spawn()
        .unwrap();
}

fn macos() {
    Command::new("open .")
        .env("PATH", "/bin")
        .spawn()
        .expect("Something went wrong, run 'open .' you forgetful knob");
}

fn main () {
    //determine os and run required function

    // We don't really need the stdout spam, but leaving this here for reference sake
    println!("explore, the overcomplicated alternative alias that nobody asked for");
    println!("looks like we're running {}", env::consts::OS);
    let os_detect = env::consts::OS;

    // working directory
    let path = env::current_dir();


    if let "macos" = os_detect {
        // add in logging because weird workflow
        println!("apple detected");
        macos();
    } else if let "windows" = os_detect {
        // add in logging because weird workflow
        println!("widnows detected");
        windows();
    }
}