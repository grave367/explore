explore

The expectation is to have a command that opens an explorer/finder/gui window from terminal at the location where it was ran.

This is purely something I wanted for my own workflow, and you could easily rename this (or just use aliases like a normal person) 

Running the command "explore" should open an explorer window at the working location. 

It's ugly as fuck, and is even dirtier, but it fits into my workflow while letting me experiment with a new language. Besides, isn't half the fun of the rust meme crowbaring it into your own workflow?


Status
Windows
Working on Windows 10

MacOS
Working on MacOS
